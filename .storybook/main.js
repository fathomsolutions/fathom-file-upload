module.exports = {
  stories: [
    '../projects/file-upload-library/**/*.stories.mdx',
    '../projects/file-upload-library/**/*.stories.@(js|jsx|ts|tsx)',
  ],
  addons: ['@storybook/addon-links', '@storybook/addon-essentials', '@storybook/addon-notes/register'],
  core: {
    builder: 'webpack5',
  },
};
