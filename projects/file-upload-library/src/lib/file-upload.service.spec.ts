import { fakeAsync, flush } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';
import { FathomFile, Page, FileService } from '@fathom/data-layer';
import { HttpClient } from '@angular/common/http';

import { FileUploadService } from './file-upload.service';

describe('FileUploadService', () => {
  let service: FileUploadService;

  const mockContent = {
    content: [
      {
        fileId: 'fileId1',
        date: 'Tue Mar 22 2022 10:57:37',
      },
      {
        fileId: 'fileId2',
        date: 'Tue Mar 20 2022 10:57:37',
      },
    ],
  } as unknown as Page<FathomFile>;
  const numberOfRecords = 10;
  const dataLayerMock = {
    getFileContent: jest.fn().mockImplementation(() => of()),
  } as unknown as FileService;

  beforeEach(() => {
    service = new FileUploadService(HttpClientTestingModule as unknown as HttpClient, dataLayerMock);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should download file', fakeAsync(() => {
    const getBlobSpy = jest.spyOn(service, 'getBlob');
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const saveFileSpy = jest.spyOn<any, any>(service, 'saveFile');
    service.downloadFile('randomId', 'fileName').subscribe(() => {
      expect(getBlobSpy).toBeCalled();
      expect(saveFileSpy).toBeCalled();
    });
    flush();
  }));

  describe('getLatestFile', () => {
    it('should perform getLatestFile properly in no files received', fakeAsync(() => {
      const getAssetDocsSpy = jest
        .spyOn(service, 'getAssetDocuments')
        .mockImplementation(() => of({ content: [] } as unknown as Page<FathomFile>));

      service.getLatestFile('randomParentId').subscribe(res => {
        expect(getAssetDocsSpy).toBeCalledWith('randomParentId', 0, numberOfRecords, null);
        expect(res).toBe(null);
      });
      flush();
    }));

    it('should perform getLatestFile properly in files received', fakeAsync(() => {
      const getAssetDocsSpy = jest.spyOn(service, 'getAssetDocuments').mockImplementation(() => of(mockContent));
      const findLatestSpy = jest.spyOn(service, 'findLatest');

      service.getLatestFile('randomParentId').subscribe(res => {
        expect(getAssetDocsSpy).toBeCalledWith('randomParentId', 0, numberOfRecords, null);
        expect(res).not.toBe(null);
        expect(findLatestSpy).toBeCalledWith(mockContent.content);
      });
      flush();
    }));
  });

  describe('findLatest', () => {
    it('findLatest should return null if no files given', () => {
      const find = service.findLatest([]);
      expect(find).toBe(null);
    });

    it('findLatest should sort files and return newest', () => {
      const find = service.findLatest(mockContent.content);
      expect(find).toEqual(mockContent.content[0]);
    });
  });
});
