import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { FileUploadService } from './file-upload.service';

@NgModule({
  imports: [CommonModule],
  declarations: [],
  providers: [FileUploadService],
  exports: [],
})
export class FileUploadModule {}
