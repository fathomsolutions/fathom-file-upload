import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { saveAs } from 'file-saver';
import { FathomFile, FileService as fileServiceDataLayer, Page } from '@fathom/data-layer';

@Injectable()
export class FileUploadService {
  update$ = new Subject();

  constructor(private http: HttpClient, private dataLayer: fileServiceDataLayer) {}

  /**
   * Get files that belong to listed resources
   * @param resourceId {string[]} - resource identifiers, often equals to assetId
   * @param skip {number} - amount of records that have to be skipped
   * @param take {number} - amount of records to take
   * @param search {string} - search files by their names
   * @param classifications {string[]} - fathom-file classifications, by default includes all specifications
   */
  getAssetDocuments(
    resourceId: string | string[],
    skip = 0,
    take = 10,
    search?: string,
    classifications?: string[],
  ): Observable<Page<FathomFile>> {
    return this.dataLayer.getResourceDocuments(resourceId, skip, take, search, classifications);
  }

  /**
   * Remove files of specific resource
   * @param resourceId {string} - delete files that belong to resource
   */
  deleteFileByResourceId(resourceId: string): Observable<void> {
    return this.dataLayer.deleteFileByResourceId(resourceId).pipe(
      tap(() => {
        this.update$.next(null);
      }),
    );
  }

  /**
   * Delete fathom-file by identifier
   * @param id {string} - unique fathom-file identifier
   */
  deleteFileById(id: string): Observable<void> {
    return this.dataLayer.deleteFileById(id).pipe(
      tap(() => {
        this.update$.next(null);
      }),
    );
  }

  /**
   * Get link to upload fathom-file
   * @param fileInfo {FathomFile} - fathom-file to upload information
   */
  getFileUploadLink(fileInfo: FathomFile): Observable<{ url: string; token: string }> {
    return this.dataLayer.getFileUploadLink(fileInfo);
  }

  /**
   * Upload fathom-file
   * @param file {File} - fathom-file to upload
   * @param relatedObjectId {string} - resource if that this fathom-file belongs to
   * @param classification {string} - fathom-file specification
   */
  uploadFileDirectly(file: File, relatedObjectId: string, classification: string): Observable<FathomFile> {
    return this.dataLayer.uploadFile(file, relatedObjectId, classification);
  }

  /**
   * Get fathom-file content
   * @param id {string} - fathom-file identifier
   */
  getBlob(id: string): Observable<Blob> {
    return this.dataLayer.getFileContent(id);
  }

  /**
   * Get fathom-file information
   * @param fileId {string} - unique fathom-file identifier
   */
  getFileInfoByFileId(fileId: string): Observable<FathomFile> {
    return this.dataLayer.getFileInfo(fileId);
  }

  /**
   * Download file by file id
   * @param id {string} - unique fathom-file identifier
   * * @param fileName {string} - name of file, optional
   */
  downloadFile(id: string, fileName?: string): Observable<Promise<void>> {
    return this.getBlob(id).pipe(map((data: BlobPart) => this.saveFile(data, fileName)));
  }

  /**
   * Sort all files by date and return latest
   * @param files {FathomFile[]} - array of files
   */
  findLatest = (files: FathomFile[]): FathomFile => {
    if (files.length === 0) return null;
    const _files = [...files];
    _files.sort((a, b) => {
      return new Date(a.date).getTime() - new Date(b.date).getTime();
    });
    return _files[_files.length - 1];
  };

  /**
   * Fetch all resource documents, filter by type and return latest of given file type
   * @param parentId {string} - resource identifiers, often equals to assetId
   * @param type {string} - optional. fathom-file classifications, by default includes all specifications
   */
  getLatestFile(parentId: string, type?: string): Observable<FathomFile> {
    const numberOfRecords = 10;
    return (
      !!type
        ? this.getAssetDocuments(parentId, 0, numberOfRecords, null, [type])
        : this.getAssetDocuments(parentId, 0, numberOfRecords, null)
    ).pipe(
      map((files: Page<FathomFile>) => {
        const fathomFiles = files?.content;

        if (fathomFiles.length === 0) return null;

        const filteredFiles = !!type
          ? fathomFiles.filter((file: FathomFile) => file.classification == type)
          : fathomFiles;
        return this.findLatest(filteredFiles);
      }),
    );
  }

  /**
   * Save file from blob part
   * @param response {BlobPart} - file content data
   * @param fileName {string} - name of file
   */
  private async saveFile(response: BlobPart, fileName: string): Promise<void> {
    const blob = new Blob([response]);

    saveAs(blob, fileName);
  }
}
