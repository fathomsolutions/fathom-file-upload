#!/bin/sh
PRECOMMIT_CONTENT=`cat .husky/pre-commit`
PRECOMMIT_COMMAND='npx lint-staged --allow-empty'

if [[ $PRECOMMIT_CONTENT != *"$PRECOMMIT_COMMAND"* ]]; then
  npx husky add .husky/pre-commit "$PRECOMMIT_COMMAND"
fi

echo "$value"
